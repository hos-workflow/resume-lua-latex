M = {}

M.info = {
	name = "",
	location = "",
	email = "",
	github = "",
	gitlab = "",
	phone = "",
	blog = "",
	linkedin = "",
	pic = "",
}

M.languages = {

	-- {
	-- 	name = "",
	-- 	desc = "",
	-- },

	{
		name = "",
		desc = "",
	},

	{
		name = "",
		desc = "",
	},

}

M.education = {
	{
		name = "",
		major = "",
		date = "",
	},
}

M.interests = {
	"",
	"",
	"",
--	"",
	"",
	"",
}

M.certifications = {

	{
		name = "",
		date = "",
		des = [[]],
	},

	{
		name = "",
		date = "",
		des = [[]],
	},

}

return M
