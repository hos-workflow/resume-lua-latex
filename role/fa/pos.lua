M = {}

-- info {{{
M.info = {
	pos = "",
	profile = [[ ]]
}
-- }}}

-- skills {{{
M.skills = {

	{
		name = "",
		details = {
			"",
			"",
			"",
			"",
			"",
		},
	},

	{
		name = "",
		details = {
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			-- "",
		},
	},

}
-- }}}

-- experience {{{
M.experiences = {

	{
		pos = "",
		name = "",
		date = "",
		loc = "",
		details = {
			"",
			"",
		},
		achieve = {
		}
	},

	{
		pos = "",
		name = "",
		date = "",
		loc = "",
		details = "",
		achieve = {
		}
	},

	{
		pos = "",
		name = "",
		date = "",
		loc = "",
		details = "",
		achieve = {
			name = "",
			columns = true,
			"",
			"",
			"",
			"",
		}
	},

}
-- }}}

-- project {{{
M.projects = {

	{
		pos = "پوزیشن",
		name = [[
            نام
		]],
		date = "حال -- ۱۴۰۰",
		loc = "محل",
		details = "جزئیات",
		achieve = {
			name = "نام",
			columns = true,
			"LAMP/LEMP",
			"Git",
			"SMTP/IMAP",
			"Reverse / Forward proxies",
		}
	},

}
-- }}}

return M
