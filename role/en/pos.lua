M = {}

-- info {{{
M.info = {
	pos = "Your desired position",
	profile =
	[[Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Ut purus elit,
vestibulum ut, placerat ac, adipiscing vitae, felis. Curabitur dictum gravida
mauris.
	]]
}
-- }}}

-- skills {{{
M.skills = {

	{
		name = "Programming",
		details = {
			"Lua",
			"C",
			"Python",
			"Bash",
			"Go",
		},
	},

	{
		name = "Tools",
		details = {
			"Linux",
			"Docker",
			"Kubernetes",
			"Jenkins",
			"Helm",
			"QEMU/KVM",
			"Makefile",
			"vim",
			"tmux",
			"git",
			"entr",
			"crontab",
			-- "LaTeX",
		},
	},

}
-- }}}

-- experience {{{
M.experiences = {

	{
		pos = "Setup various Self-hosting services on VPS",
		name = [[
			Database management
		]],
		date = "2019 -- Now",
		loc = "Remote",
		details = [[
			Setup and support many services like LAMP/LEMP,
			SMTP/IMAP/POP and git on bare metal/virtual servers.
		]],
		achieve = {
		}
	},

	{
		pos = "Linux System Administrator and Automation",
		name = "RPS Cloud",
		date = "1401",
		loc = "Remote",
		details = [[VPS and Cloud service provider]],
		achieve = {
		}
	},

	{
		pos = "DevOps mentoring and Linux bootcamp",
		name = [[Linux, Docker, Kubernetes, Helm, CI/CD]],
		date = "2021 -- Now",
		loc = "BigBlueButton",
		details = [[]],
		achieve = {
			name = "Books that have been read",
			columns = true,
			"LPIC-1",
			"Docker Deep Dive",
			"Kubernetes in Action",
			"Pipeline as Code",
		}
	},

}
-- }}}

-- project {{{
M.projects = {

	{
		pos = "Position",
		name = [[
            Name
		]],
		date = "2020 -- Now",
		loc = "Location",
		details = "Some details",
		achieve = {
			name = "Services",
			columns = true,
			"LAMP/LEMP",
			"Git",
			"SMTP/IMAP",
			"Reverse / Forward proxies",
		}
	},

}
-- }}}

return M
