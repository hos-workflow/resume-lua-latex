#!/bin/sh

path="/home/hos/dev/primejade/hos-workflow/scripts"
prompt="$(echo $0 | awk -F '/' '{print $NF;}')"

iname='tmp-tmp.png'
oname='tmp.png'
sqrp='10'

# argument parsing {{{
while [ "${#}" -gt 0 ]; do
	case ${1} in
		-i|--input)
			input="${2}"
			shift
			;;
		-o|--output)
			output="${2}"
			shift
			;;
		*)
			echo "[${prompt}]: Unknown parameter passed: ${1}"
			exit 1
			;;
	esac
	shift
done

if [ -z "${input}" ]; then
	input="${iname}"
fi

if [ -z "${output}" ]; then
	output="${oname}"
fi
# }}}

## get dementions:
# identify me-temp.png | awk '{print $3;}'
res=$(identify ${iname} \
	| awk '{print $3;}')
dmax=$(echo ${res} \
	| sed 's/x.*//')
dmid=$(echo "${dmax} / 2 " | bc)

# circle {{{
circle() {
	# argument parsing {{{
	func_name="circle"
	while [ "${#}" -gt 0 ]; do
		case ${1} in
			-i|--input)
				input="${2}"
				shift
			;;
			-o|--output)
				output="${2}"
				shift
			;;
			*)
				echo "[${prompt}]: ${func_name}: Unknown parameter passed: ${1}"
				exit 1
			;;
		esac
		shift
	done
	
	if [ -z "${input}" ]; then
		input="${iname}"
	fi
	
	if [ -z "${output}" ]; then
		output="${oname}"
	fi

	# }}}

	convert ${input} \
		-gravity Center \
		\( \
			-size ${res} \
			xc:None \
			-fill White \
			-draw \
				"roundrectangle 0,0 ${dmax},${dmax} ${dmid},${dmid}" \
			-alpha Copy \
		\) \
		-compose CopyOpacity -composite \
		-trim ${output}
}
# }}}

# square {{{
square() {
	# argument parsing {{{
	func_name="circle"
	while [ "${#}" -gt 0 ]; do
		case ${1} in
			-i|--input)
				input="${2}"
				shift
			;;
			-o|--output)
				output="${2}"
				shift
			;;
			*)
				echo "[${prompt}]: ${func_name}: Unknown parameter passed: ${1}"
				exit 1
			;;
		esac
		shift
	done
	
	if [ -z "${input}" ]; then
		input="${iname}"
	fi
	
	if [ -z "${output}" ]; then
		output="${oname}"
	fi

	# }}}

	convert ${input} \
		-gravity Center \
		\( \
			-size ${res} \
			xc:Black \
			-fill White \
			-draw \
				"roundrectangle 0,0 ${dmax},${dmax} ${sqrp},${sqrp}" \
			-alpha Copy \
		\) \
		-compose CopyOpacity -composite \
		-trim ${output}
}
# }}}

square
# circle

imagemagick-shadow -i ${oname} -o ${oname}
# imagemagick-shadow -i ${oname} -o ${oname} -bw 0.1 -bc white
imagemagick-trim -i ${oname} -o ${oname}

