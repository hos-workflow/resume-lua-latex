M = {
	fg         = '#000000',
	bg         = '#ffffff',
	cf         = '#232323',
	cb         = '#eeeeee',
	link       = '#101010',
	date       = '#101010',
	section    = '#101010',
	title      = '#101010',
	error      = '#101010',
	subsection = '#101010',
}

return M
