M = {
	fg         = '#e2e9e9',
	bg         = '#0F111A',
	cf         = '#B0BEC5',
	cb         = '#1F2233',
	link       = '#71C6E7',
	date       = '#ABCF76',
	section    = '#B480D6',
	title      = '#6E98EB',
	error      = '#DC6068',
	subsection = '#E6B455',
}

return M
