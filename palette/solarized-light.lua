M = {
	fg         = '#002b36',
	bg         = '#fdf6e3',
	cf         = '#073642',
	cb         = '#eee8d5',
	link       = '#6c71c4',
	date       = '#dc322f',
	section    = '#859900',
	title      = '#268bd2',
	error      = '#d33682',
	subsection = '#b58900',
}

return M
