M = {
	fg         = '#000000',
	bg         = '#ffffff',
	cf         = '#232323',
	cb         = '#ededed',
	link       = '#5f0000',
	date       = '#005f00',
	section    = '#005f5f',
	title      = '#00005f',
	error      = '#5f005f',
	subsection = '#5f5f00',
}

return M
