M = {
	fg         = '#eee8d5',
	bg         = '#002b36',
	cf         = '#93a1a1',
	cb         = '#073642',
	link       = '#6c71c4',
	error      = '#dc322f',
	date       = '#859900',
	title      = '#268bd2',
	section    = '#d33682',
	subsection = '#b58900',
}

return M
