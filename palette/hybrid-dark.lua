M = {
	fg         = '#e4e4e4',
	bg         = '#1d1f21',
	cf         = '#c5c8c6',
	cb         = '#373b41',
	link       = '#cc6666',
	date       = '#b5bd68',
	section    = '#8abeb7',
	title      = '#81a2be',
	error      = '#b294bb',
	subsection = '#f0c674',
}

return M
