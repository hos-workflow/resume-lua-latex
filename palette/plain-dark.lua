M = {
	fg         = '#ffffff',
	bg         = '#000000',
	cf         = '#eeeeee',
	cb         = '#232323',
	link       = '#efefef',
	date       = '#efefef',
	section    = '#efefef',
	title      = '#efefef',
	error      = '#efefef',
	subsection = '#efefef',
}

return M
