M = {
	fg         = '#eae8ff',
	bg         = '#191726',
	cf         = '#cdcbe0',
	cb         = '#2d2a45',
	link       = '#7bb8c1',
	date       = '#8aa872',
	section    = '#a580d2',
	title      = '#4a869c',
	error      = '#d84f76',
	subsection = '#e6a852',
}

return M
