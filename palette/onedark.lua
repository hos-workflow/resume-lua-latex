M = {
	fg         = '#dddddd',
	bg         = '#21252b',
	cf         = '#abb2bf',
	cb         = '#31353f',
	link       = '#56b6c2',
	date       = '#98c379',
	section    = '#c678dd',
	title      = '#61afef',
	error      = '#e86671',
	subsection = '#e5c07b',
}

return M
