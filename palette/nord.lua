M = {
	fg         = '#eceff4',
	bg         = '#2e3440',
	cf         = '#d8dee9',
	cb         = '#434c5e',
	link       = '#8fbcbb',
	date       = '#a3be8c',
	section    = '#b48ead',
	title      = '#5e81ac',
	error      = '#bf616a',
	subsection = '#ebcb8b',
}

return M
