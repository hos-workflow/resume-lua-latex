M = {
	fg         = '#f8f8f2',
	bg         = '#191A21',
	cf         = '#ABB2BF',
	cb         = '#242630',
	link       = '#8be9fd',
	date       = '#50fa7b',
	section    = '#ff79c6',
	title      = '#bd93f9',
	error      = '#ff5555',
	subsection = '#f1fa8c',
}

return M
