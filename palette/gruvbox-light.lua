M = {
	fg         = '#1d2021',
	bg         = '#f9f5d7',
	cf         = '#282828',
	cb         = '#f2e5bc',
	date       = '#d79921',
	error      = '#98971a',
	link       = '#689d6a',
	section    = '#b16286',
	subsection = '#cc241d',
	title      = '#458588',
}

return M
