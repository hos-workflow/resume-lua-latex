M = {
	fg         = '#ebdbb2',
	bg         = '#1d2021',
	cf         = '#928374',
	cb         = '#282828',
	date       = '#cc241d',
	error      = '#689d6a',
	link       = '#458588',
	section    = '#d79921',
	subsection = '#98971a',
	title      = '#b16286',
}

return M
