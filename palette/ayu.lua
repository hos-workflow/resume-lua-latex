M = {
	fg         = '#fafafa',
	bg         = '#101521',
	cf         = '#c7c7c7',
	cb         = '#232834',
	link       = '#90e1c6',
	date       = '#c2d94c',
	section    = '#fae994',
	title      = '#53bdfa',
	error      = '#ea6c73',
	subsection = '#f9af4f',
}

return M
