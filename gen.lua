#!/usr/bin/env lua

M = {}

local current_language = 'en'
local info_path = 'head'
local role_path = 'pos'
-- local current_theme = 'color'
local current_theme = 'plain'
local random_theme = false
local pic_path = 'tmp.png'

-- fonts {{{
local fonts = {
	en = {
		main = "Source Sans Pro",
		sans = "Ubuntu",
		mono = "Fira Sans",
	},
	fa = {
		latintext = "Fira Sans",
		text = "Sahel",
		s = "Sahel Black",
		q = "Sahel",
	},
}
-- }}}

-- backend variables {{{
local output_file = 'resume.tex'
local base = require(string.format("role.%s.%s", current_language, info_path))
local template = require(string.format("role.%s.%s", current_language, role_path))
-- local status, template
-- status, template = pcall(require, string.format("role.%s.%s", current_language, role_path))
-- if status == false then
-- 	template = require(string.format('role.%s.template', current_language))
-- end
local list_themes = {
	'ayu',
	'dracula',
	'gruvbox-dark',
	'gruvbox-light',
	'hybrid-dark',
	'hybrid-light',
	'material',
	'nightfox',
	'nord',
	'onedark',
	'plain',
	'plain-dark',
	'solarized-dark',
	'solarized-light',
}
if random_theme then
	current_theme = list_themes[math.random(1, #list_themes)]
end
-- }}}

-- user info {{{
local info = {
	user = {
		name = base.info.name,
		location = base.info.location,
		email = base.info.email,
		github = base.info.github,
		gitlab = base.info.gitlab,
		phone = base.info.phone,
		blog = base.info.blog,
		linkedin = base.info.linkedin,
		pic = pic_path or base.info.pic,
		pos = template.info.pos,
		profile = template.info.profile,
	},
	skills = template.skills,
	experiences = template.experiences,
	projects = template.projects,
	educations = base.education,
	languages = base.languages,
	interests = base.interests,
	certificates = base.certifications,
}
-- }}}

-- gen theme {{{
local function gen_theme(name)
	if name == nil then
		name = current_theme
	end
	local theme = require(string.format('palette.%s', name))
	local f = io.open('theme.sty', 'w')
	if f == nil then
        return
	end

	f:write('\\NeedsTeXFormat{LaTeX2e}\n')
	f:write('\\ProvidesPackage{draculatheme}[2021/02/26 Dracula Theme]\n')
	f:write('\\RequirePackage{xcolor}\n')

	for key, value in pairs(theme) do
		value = string.gsub(value, '^#', '')
		f:write(string.format('\\definecolor{scheme%s}{HTML}{%s}\n', key, value))
	end

	f:write('\\pagecolor{schemebg}\n')
	f:write('\\color{schemefg}\n')

	f:close()
end
-- }}}

-- fonts {{{
local function setfonts(file, list, lang)
	local en_names = {
		"main",
		"sans",
		"mono",
	}

	local fa_names = {
		"text",
		"latintext",
	}

	if lang == "en" then
		for _, v in pairs(en_names) do
			file:write(string.format("\\set%sfont{%s}\n", v, list.en[v]))
		end
		-- file:write()
	else
		for _, v in pairs(fa_names) do
			file:write(string.format("\\set%sfont{%s}\n", v, list.fa[v]))
		end

		file:write(string.format("\\defpersianfont\\sfont{%s}\n", list.fa["s"]))
		file:write(string.format("\\defpersianfont\\qfont{%s}\n", list.fa["q"]))
	end
end
-- }}}

-- persian {{{
local function xepersian(f)
local persian = [[
\usepackage{xepersian}
]]
f:write(persian)
end
-- }}}

-- main latex {{{

-- pre {{{
local prebody = [[
\documentclass[a4paper]{article}
%\usepackage{showframe} % debug
% preables 
\usepackage{theme}
\usepackage[margin=4mm]{geometry}
\usepackage{xcolor,fontspec,fancyvrb,titlesec,framed,listings,tabularx}
% config {{{
% lstlisting formatting {{{
\lstset{frame=tb,
	showstringspaces=false,
	% columns=flexible,
	% numbers=left,
	% xleftmargin=.03\textwidth,
	% xrightmargin=.03\textwidth,
	basicstyle={\ttfamily},
	keywordstyle={},
	ndkeywordstyle={},
	commentstyle={},
	emphstyle={},
	stringstyle={},
	numberstyle={\ttfamily\scriptsize},
	rulecolor=\color{black!50!white},
	breaklines=true,
	breakatwhitespace=false,
	frame=shadowbox,
	captionpos=t,
	tabsize=6
}
% end code block formatting }}}
\usepackage{tikz}
\usepackage[most]{tcolorbox}
\linespread{1.4}
% }}}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{fontawesome5}
\usepackage{hyperref}
\hypersetup{
	colorlinks=true,
	linkcolor={schemelink},
	urlcolor={schemelink},
	citecolor={schemelink},
	pdftitle={resume},
}
\usepackage[most]{tcolorbox}
\newtcolorbox{ebox}{%
	arc=4pt,
	beamer,
	before upper={\rule[-3pt]{0pt}{10pt}},
	bottom=0pt,
	boxrule=0pt,
	boxsep=0pt,
	colback=schemecb,
	colframe=schemecb,
	colupper=schemecf,
	left=0pt,
	right=0pt,
	top=0pt,
    on line,
}
\newtcolorbox{infobox}[1][]{
	% attach boxed title to top left={yshift=-2mm, xshift=1cm},
	% attach boxed title to top center={yshift=-3pt},
	attach boxed title to top center={yshift=-1pt},
	boxed title style={%
		skin=enhancedfirst jigsaw,
		size=small,
		% beamer,
		arc=10pt,
		% sharp corners = downhill,
		bottom=-0mm,
		interior style={%
			fill=none,
			top color=schemecb,
			bottom color=schemecb,
		},
	},
	minipage boxed title*=-15cm,
	enlarge top by=0mm,
	% sharp corners = uphill,
	arc = 00pt,
	toprule=0pt,
	boxrule=0pt,
	titlerule=0.5pt,
	center,
	% beamer,
	colback  = schemecb,
	colbacktitle = schemecb,
	colupper = schemecf,
	collower = schemecf,
	coltitle = schemecf,
	colframe = schemecb,
	% bottom=1mm,
	enhanced,
	interior style={top color=schemebg,bottom color=schemecb},
    title={#1},
}
\newtcbox{\cbox}[1][purple]{%
	on line,
	colupper=schemecf,
	arc=3pt,
	colback=schemecb,
	colframe=schemecb,
	before upper={\rule[-3pt]{0pt}{10pt}},
	boxrule=1pt,
	boxsep=0pt,
	left=6pt,
	right=6pt,
	top=3pt,
	bottom=1pt,
	% --
	toprule=0pt,
	boxrule=0.5pt,
	titlerule=0pt,
	colback  = schemecb,
	colbacktitle = schemecb,
	colupper = schemefg,
	coltitle = schemefg,
	colframe = schemecb,
	enhanced,
	interior style={top color=schemebg,bottom color=schemecb},
}
\newtcbox{\lbox}[1][purple]{%
    on line,
	colupper=schemecf,
	arc=0pt,
	colback=schemecb,
	colframe=schemecb,
	before upper={\rule[-3pt]{0pt}{10pt}},
	boxrule=1pt,
	boxsep=0pt,
	left=6pt,
	right=6pt,
	top=2pt,
	bottom=2pt,
	% --
	toprule=0pt,
	boxrule=0pt,
	titlerule=0pt,
	colback  = schemecb,
	colbacktitle = schemecb,
	colupper = schemecf,
	coltitle = schemecf,
	colframe = schemecf,
	enhanced,
	interior style={top color=schemecb,
	bottom color=schemebg},
}
\newtcbox{\rbox}[1][purple]{%
    on line,
	colupper=schemecb,
	arc=0pt,
	colback=schemecf,
	colframe=schemecf,
	before upper={\rule[-3pt]{0pt}{10pt}},
	boxrule=1pt,
	boxsep=0pt,
	left=6pt,
	right=6pt,
	top=2pt,
	bottom=2pt,
	% --
	toprule=0pt,
	boxrule=0pt,
	titlerule=0pt,
	colback  = schemecb,
	colbacktitle = schemecb,
	colupper = schemecf,
	coltitle = schemecf,
	colframe = schemecf,
	enhanced,
	interior style={top color=schemecb,
	bottom color=schemebg},
}
% we use paracol to display breakable two columns
\usepackage{paracol}
\usepackage{fancyhdr}
\pagestyle{empty}
% indentation is zero
\setlength{\parindent}{0mm}
% extended aligning of tabular cells
\usepackage{array}
% custom column right-align with fixed width
% use like p{size} but via x{size}
\newcolumntype{x}[1]{%
>{\raggedleft\hspace{0pt}}p{#1}}%
% hline width
\newcommand{\thehlinewidth}{1pt}
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\usepackage{titlesec}
\usepackage{titling}
\titleformat{\section} {\large\bfseries\color{schemefg}}{}{0em}{}[{\titlerule[0pt]}]
\titlespacing{\section}{0pt}{20pt}{15pt}

\titleformat{\subsection} {\bfseries}{}{0em}{}[{\titlerule[0pt]}]
\titlespacing{\subsection}{0pt}{5pt}{3pt}
]]
-- }}}
local begindocument = '\\begin{document}'
local enddocument = '\\end{document}'
-- local sep = "\n\n\\par\\rule{\\textwidth}{\\thehlinewidth}\n\n"
-- local sep = "\n\n"
-- pre post {{{
local function _post(sepratio, sepwidth, file_name)
    if current_language == "fa" then
        sepratio = 1 - sepratio
    end
	local writeit = string.format([[\columnratio{%s}
\setlength{\columnsep}{%sem}
\setlength{\columnseprule}{0pt}
\colseprulecolor{schemebg}
\begin{paracol}{2}]], sepratio, sepwidth)
	file_name:write(writeit)
end

local function r_post(file_name)
	local writeit = string.format([[\begin{rightcolumn}]])
	file_name:write(writeit)
end
local function post_r(file_name)
	local writeit = string.format([[\end{rightcolumn}]])
	file_name:write(writeit)
end

local function post_(file_name)
	local writeit = [[\end{paracol}]]
	file_name:write(writeit)
end

local function l_post(file_name)
	local writeit = string.format([[\begin{leftcolumn}]])
	file_name:write(writeit)
end
local function post_l(file_name)
	local writeit = string.format([[\end{leftcolumn}]])
	file_name:write(writeit)
end
-- }}}
-- }}}

-- base functions {{{

-- saperator {{{
local function separator(file, length)
	if length == nil then
		length = 5
	end
	if length == 0 then
		file:write('\n\n')
	elseif type(length) == 'string' then
		file:write(string.format('\\\\[%s]\n', length))
	else
		file:write(string.format('\n\\vspace{%spt}\n\n', length))
	end
end
-- }}}

-- text format {{{

-- local function underline(txt)
-- 	return string.format('%s', txt)
-- 	-- return string.format('\\underline{%s}', txt)
-- end

local function monospace(txt)
	-- return string.format('%s', txt)
    if current_language == "fa" then
        return string.format('{\\small %s}', txt)
    else
        return string.format('\\texttt{\\small %s}', txt)
    end
end

local function italics(txt)
	-- return string.format('%s', txt)
	return string.format('\\textit{%s}', txt)
end

local function bold(txt)
	return string.format('\\textbf{%s}', txt)
end

-- local function large(txt)
-- 	return string.format('{\\large %s}', txt)
-- end

-- local function llarge(txt)
-- 	return string.format('{\\Large %s}', txt)
-- end

-- local function lllarge(txt)
-- 	return string.format('{\\LARGE %s}', txt)
-- end

local function color(txt, colour)
	return string.format('\\textcolor{scheme%s}{%s}', colour, txt)
end

-- local function tcolor(txt)
-- 	return string.format('\\lbox{%s} ', txt)
-- end

-- local function vcolor(txt)
-- 	return string.format('\\rbox{%s} ', txt)
-- end

-- local function sans(txt)
-- 	return string.format('\\textsf{%s} ', txt)
-- end

local function box(txt)
	return string.format('\\cbox{%s} ', txt)
end

local function hfill(file)
	file:write('\\hfill ')
end

-- local function vfill()
-- 	file:write('\\vfill ')
-- end
-- }}}

-- latin on persian helper {{{
local function latin(url)
	if current_language == 'fa' then
		return string.format('\\lr{%s}', url)
	else
		return url
	end
end

-- local function latinenv(url)
-- 	if current_language == 'fa' then
-- 		return string.format('\\begin{latin}\n\\lr{%s}\n\\end{latin}\n', url)
-- 	else
-- 		return url
-- 	end
-- end

local function clang(strings)
	if current_language == 'fa' then
		return string.format('%s', strings[2])
	else
		return string.format('%s', strings[1])
	end
end
-- }}}

-- section {{{
local function section(file, name)
	-- local font = ''
	-- if current_language == 'fa' then
	-- 	font = '\\sfont'
	-- end
	-- separator(file, 10)
	-- file:write(string.format('\n{\\scshape%s%s}', font,
	--                  lllarge(bold(color(name, 'section')))))

	file:write(string.format('\\section{%s}\n', name))
end
-- }}}

-- }}}

-- write functions {{{

-- user information {{{
local function userinfo(theinfo, file)
	local sfont = ''
	local qfont = ''
	if current_language == 'fa' then
		sfont = '\\sfont'
		qfont = '\\qfont'
	end
	local name = string.format("{\\bfseries%s %s}", sfont, theinfo.user.name)
	local position = string.format('{%s %s}', qfont, theinfo.user.pos)
	local pic = theinfo.user.pic
	-- local profile = theinfo.user.profile

	local contacts = {

		{
			name = "Github",
			icon = "github",
			desc = string.format('\\href{https://github.com/%s}{%s}',
				theinfo.user.github, monospace(latin(theinfo.user.github))),
		},

		{
			name = "Gitlab",
			icon = "gitlab",
			desc = string.format('\\href{https://gitlab.com/%s}{%s}',
				theinfo.user.github, monospace(latin(theinfo.user.gitlab))),
		},

		{
			name = "Linkedin",
			icon = "linkedin",
			desc = string.format("\\href{https://linkedin.com/in/%s}{%s}",
				theinfo.user.linkedin, monospace(latin(theinfo.user.linkedin))),
		},

		{
			name = "Blog",
			icon = "globe-americas",
			desc = string.format("\\href{https://%s}{%s}",
				theinfo.user.blog, monospace(latin(theinfo.user.blog)))
		},

		{
			name = "Phone",
			icon = "mobile-alt",
			desc = string.format("%s", monospace(latin(theinfo.user.phone))),
		},

		{
			name = "Email",
			icon = "envelope",
			desc = string.format('\\href{mailto:%s}{%s}', theinfo.user.email, monospace(latin(theinfo.user.email))),
		},

		{
			name = "Location",
			icon = "map-marker-alt",
			desc = monospace(theinfo.user.location),
		},

	}

	-- if theinfo.user.blog ~= "" then
	-- 	table.insert(contacts_l, {
	-- 		name = "Blog",
	-- 		icon = "globe-americas",
	-- 		desc = string.format("\\href{https://%s}{%s}",
	-- 			theinfo.user.blog, monospace(latin(theinfo.user.blog)))
	-- 	})
	-- end

	-- _post(first_sep, second_sep, file)
	-- 	l_post(file)
		-- post_l(file)

		-- r_post(file)
			file:write('\\begin{center}\n')
			file:write(string.format('\n\\includegraphics[width=0.5\\linewidth]{%s}\n', pic))
			file:write('\\\\')
			file:write(string.format('{\\Large\\textcolor{schemetitle}{%s}}', name))
			file:write('\\\\')
			file:write(string.format('{\\large\\bfseries\\textcolor{schemecf}{%s}} \\\\[10pt]\n', position))
			file:write('\\end{center}\n')
			-- file:write(string.format('{%s}\n', profile))
			-- file:write('\\hspace{5mm}')
	-- 	post_r(file)
	-- post_(file)

	-- file:write(sep)
	-- separator(file, '10')
	-- _post(second_sep, file)

	-- file:write('\\setlength\\columnseprule{0pt}\n')

	-- file:write('\\begin{quote}\n')

	-- file:write(string.format('\\begin{infobox}[\\centering\\textbf{%s}]\n',
	-- 	        clang{'Contact', 'اطلاعات تماس'}))

	-- file:write('\\tcblower\n')
	-- file:write('\\begin{quote}\n')
    section(file, color(clang({'Contact Info', 'اطلاعات تماس'}), 'section'))
	file:write('\\begin{tabularx}{\\linewidth}{cXr}\n')
        for _, value in pairs(contacts) do
            file:write(string.format("\\faIcon{%s} & %s & \\textbf{%s} \\\\\n",
                value.icon, value.name, value.desc))
        end
	file:write('\\end{tabularx}\n')
	separator(file, 20)
end
-- }}}

-- languages {{{
local function language(input_list, file)
	file:write('\\begin{infobox}\n')
	section(file, color(clang({'Languages', 'زبان‌ها'}), 'section'))
	for _,value in pairs(input_list) do
		file:write(color(bold(value.name), 'subsection'), '\n')
		file:write('\\dotfill')
		file:write('\\space\n')
		file:write(box(value.desc))
		file:write('\n\n')
	end
	file:write('\\end{infobox}\n')
end
-- }}}
-- educations {{{
local function education(input_list, file)
	section(file, color(clang({'Educations', 'تحصیلات'}), 'section'))
	for _,value in pairs(input_list) do
		file:write(color(bold(value.name), 'subsection'), '\n')
		file:write('\\dotfill')
		file:write('\\space\n')
		file:write(box(value.date))
		file:write('\n\n')
		file:write(color(value.major, 'cf'))
		file:write('\n\n')
	end
end
-- }}}
-- certificates {{{
local function certification(input_list, file)
	section(file, color(clang({'Certifications', 'مدرک‌ها'}), 'section'))
	for _,value in pairs(input_list) do
		-- file:write('\\ \\ $\\bullet$\\ \\ \n')
		file:write(bold(latin(value.name)), '\n')
		file:write('\\dotfill')
		file:write('\\space\n')
		file:write(box(value.date))
		file:write('\n\n')
	end
end
-- }}}
-- -- interests {{{
-- local function interest(input_list, file)
-- 	section(file, color(clang({'Interests', 'علاقه‌مندی‌ها'}), 'section'))
-- 	-- separator(file, '4pt')
-- 	for key,value in pairs(input_list) do
-- 		file:write(box(value))
-- 		if key % 3 == 0 then
-- 			separator(file, '1pt')
-- 		end
-- 	end
-- end
-- -- }}}

-- skills {{{
local function skill(input_list, file)
	file:write('\\begin{infobox}\n')
	section(file, color(clang({'Skills', 'مهارت‌ها'}), 'section'))
	-- separator(file, '9pt')
	for idx, value in pairs(input_list) do
		separator(file, 1)
		file:write(color(bold(value.name), 'subsection'))

        if current_language == 'fa' then
            file:write("\\vspace{4pt}\n")
            file:write("\\begin{latin}\n")
        else
            separator(file, '4pt')
        end

		for k,v in pairs(value.details) do
            file:write(box(v))
            if value.name == clang({"Programming", 'برنامه‌نویسی'}) then
                if k % 6 == 0 then
                    if k ~= #value.details then
                        separator(file, '4pt')
                    end
                end
            else
                if k % 6 == 0 then
                    if k ~= #value.details then
                        separator(file, '4pt')
                    end
                end
            end
		end

        if idx ~= #input_list then
            if current_language == 'fa' then
                file:write("\\end{latin}\n")
                file:write("\\vspace{8pt}\n")
            else
                -- separator(file, 10)
                file:write("\\vspace{8pt}\n")
            end
        end

		-- separator(file, 12)
	end
	file:write('\\end{infobox}\n')
end
-- }}}
-- experiences {{{
local function experience(input_list, file)
	file:write('\\begin{infobox}\n')
	section(file, color(clang({'Experiences', 'سابقه ﺷﻐﻠﯽ'}), 'section'))

	for _, value in pairs(input_list) do
		local position = value.pos
		local name = italics(value.name)
		local date = bold(value.date)
		local location = italics(value.loc)
		local description = value.details
		local achieve = value.achieve

		if position ~= nil then
			file:write(bold(color(position, 'subsection')), '\n')
			hfill(file)
			file:write(color(date, 'date'))
			separator(file, 1)
		end

		if location ~= nil then
			file:write(color(name, 'fg'))
			hfill(file)
			file:write(color(location, 'fg'))
			separator(file, 0)
		end

		if description == nil or description == '' then
			file:write(color(description, 'cf'))
			file:write(string.format('\n\\vspace{%spt}\n', 0))
		else
			file:write(color(description, 'cf'))
			file:write(string.format('\n\\vspace{%spt}\n\n', 0))
		end

		if next(achieve) ~= nil then
			if achieve.name == '' or achieve.name == nil then
				file:write(bold(clang({'Achievements', 'توضیحات'})))
				separator(file, 0)
			else
				file:write(bold(achieve.name))
			end

			local multicol = achieve.columns or false
			if multicol == true then
				-- separator(file, -15)
				file:write('\\setlength\\columnseprule{0pt}\n')
				file:write('\\begin{multicols}{2}\n')
				for k,v in ipairs(achieve) do
					file:write('\\quad$\\bullet$\\quad ')
					file:write(latin(v), '\n')
					file:write('\n\n')

					if k == (#achieve // 2) then
						print(k, #achieve // 2)
						file:write('\\columnbreak\n')
					end

				end
				file:write('\\end{multicols}\n')
				separator(file, -10)
			else
				-- separator(file, -10)
				file:write('\\setlength\\columnseprule{0pt}\n')
				for _,v in ipairs(achieve) do
					file:write('\\quad$\\bullet$\\quad ')
					file:write(v, '\n')
					file:write('\n\n')
				end
			end
		end

	-- separator(file, 10)
	-- separator(file, 10)
	separator(file, 15)
	end
	file:write('\\end{infobox}\n')
end
-- }}}
-- projects {{{
local function project(input_list, file)
	separator(file, -10)
	separator(file, -10)

	file:write('\\begin{infobox}\n')
	section(file, color(clang({'Projects', 'پروژه‌ها'}), 'section'))

	for _, value in pairs(input_list) do
		local position = value.pos
		local name = italics(value.name)
		local date = bold(value.date)
		local location = italics(value.loc)
		local description = value.details
		local achieve = value.achieve

		if position ~= nil then
			file:write(bold(color(position, 'subsection')), '\n')
			hfill(file)
			file:write(color(date, 'date'))
			separator(file, 0)
		end

		if location ~= nil then
			file:write(color(name, 'fg'))
			hfill(file)
			file:write(color(location, 'fg'))
			separator(file, 0)
		end

		if description == nil or description == '' then
			file:write(color(description, 'cf'))
			file:write(string.format('\n\\vspace{%spt}\n', 0))
		else
			file:write(color(description, 'cf'))
			file:write(string.format('\n\\vspace{%spt}\n\n', 0))
		end

		-- separator(file, 0)
		if next(achieve) ~= nil then
			if achieve.name == '' or achieve.name == nil then
				file:write("\\ ", bold(clang({'Achievements', 'توضیحات'})))
				separator(file, 0)
			elseif achieve.name == " " then
				-- file:write(bold(clang({'Achievements', 'توضیحات'})))
			else
				file:write("\\ ", bold(achieve.name))
			end

			local multicol = achieve.columns or false
			if multicol == true then
				-- separator(file, -15)
    --                 separator(file, 10)
				file:write('\\setlength\\columnseprule{0pt}\n')
				file:write('\\begin{multicols}{2}\n')
				for k,v in ipairs(achieve) do
					file:write('\\quad$\\bullet$\\quad ')
					file:write(latin(v), '\n')
					file:write('\n\n')

					if k == (#achieve // 2) then
						print(k, #achieve // 2)
						file:write('\\columnbreak\n')
					end

				end
				file:write('\\end{multicols}\n')
				-- separator(file, -10)
			else
				-- separator(file, -10)
				file:write('\\setlength\\columnseprule{0pt}\n')
				for _,v in ipairs(achieve) do
					file:write('\\quad$\\bullet$\\quad ')
					file:write(v, '\n')
					file:write('\n\n')
				end
			end
		end

	-- separator(file, 10)
	-- separator(file, 10)
	end
	file:write('\\end{infobox}\n')
	-- file:write('\\begin{infobox}\n')
end
-- }}}
-- about {{{
local function about(input_data, file)
	separator(file, -10)
	separator(file, -10)

	file:write('\\begin{infobox}\n')
	section(file, color(clang({'About Me', 'درباره من'}), 'section'))
	file:write(string.format('{%s}\n', input_data))

	file:write('\\end{infobox}\n')
end
-- }}}

-- }}}

-- main {{{
function M.Build(theme)
	gen_theme(theme)
	local sep_length = 10
	local file = io.open(output_file, "w") -- Open output file in write mode
    if file == nil then
        return
    end
	file:write(prebody)
	-- os.exit(2)
	if current_language == 'fa' then
		xepersian(file)
	end
	setfonts(file, fonts, current_language)
	file:write(string.format('%s\n', begindocument))

	info.w = '0.21'
	info.x = '0.7'
	info.y = '1.5'
	info.z = '0.35'

	-- separator(file, -20)

	_post(info.z, info.y, file)

    if current_language == "fa" then
        l_post(file)
            -- separator(file, sep_length)
            language(info.languages, file)
            -- separator(file, sep_length)
            skill(info.skills, file)
            experience(info.experiences, file)
            -- separator(file, -3 * sep_length)
            project(info.projects, file)
            separator(file, sep_length)
            about(info.user.profile, file)
        post_l(file)
        file:write("\\hfill‌")

        r_post(file)
            file:write('\\begin{infobox}\n')
            userinfo(info, file)
            -- os.exit(1)
            separator(file, -3 * sep_length)
            education(info.educations, file)
            certification(info.certificates, file)
            -- separator(file, sep_length)
            -- interest(info.interests, file)
            -- separator(file, sep_length)
            file:write('\\end{infobox}\n')
        post_r(file)
        post_(file)
    else
        file:write('\\begin{infobox}\n')
        l_post(file)
            userinfo(info, file)
            -- os.exit(2)
            separator(file, -2 * sep_length)
            education(info.educations, file)
            certification(info.certificates, file)
            -- separator(file, sep_length)
            -- interest(info.interests, file)
            -- separator(file, sep_length)
        post_l(file)
        file:write("\\hfill‌")
        file:write('\\end{infobox}\n')

        r_post(file)
            -- separator(file, sep_length)
            language(info.languages, file)
            -- separator(file, sep_length)
            skill(info.skills, file)
            experience(info.experiences, file)
            -- separator(file, -2 * sep_length)
            project(info.projects, file)
            separator(file, sep_length)
            about(info.user.profile, file)
        post_r(file)
        post_(file)
    end

	file:write(string.format('%s\n', enddocument))
	file:close() -- Close the file
end
-- }}}

-- resume()

return M

-- for _, value in ipairs(list_themes) do
-- 	resume(value)
-- 	os.execute('xelatex resume.tex')
-- 	os.execute(string.format('mv resume.pdf %s-resume.pdf', value))
-- 	os.execute('sleep 1')
-- end

